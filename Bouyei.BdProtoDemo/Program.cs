﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bouyei.BdProto;
using Bouyei.BdProto.Structures;
using Bouyei.BdProto.JT808;
using Bouyei.BdProto.JT808.Request;
using Bouyei.BdProto.JT808.Reponse;
using Bouyei.BdProto.Providers;

namespace Bouyei.BdProtoDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            InterfaceEncoding();
            ManualEncoding();

            //接收到终端数据包后使用一下方法解析数据包
            InterfaceParser(null);
            ManualParser(null);
        }

        private static void InterfaceEncoding()
        {
            IPacketProvider provider = PacketProvider.CreateProvider();
            var body = new PB8001()
            {
                MessageId = JT808Cmd.RSP_0102,
                Serialnumber = 0,
                Result = 0
            };
            ReqParam req = new ReqParam()
            {
                msgSerialnumber = 0,
                pEncryptFlag = 0,
                pSerialnumber = 1,
                pSubFlag = 0,
                pTotal = 1,
                simNumber = "18212001111"
            };

            var buffer = new Req8001Packet(provider).Encode(body, req);
        }

        private static void ManualEncoding()
        {
            IPacketProvider provider = PacketProvider.CreateProvider();
            string phone = "18212001111";

            //终端连接鉴权平台回复通用应答
            byte[] body = new REQ_8001().Encode(new PB8001()
            {
                MessageId = JT808Cmd.RSP_0102,
                Serialnumber = 0,
                Result = 0
            });

            byte[] buffer = provider.Encode(new PacketFrom()
            {
                msgBody = body,
                msgId = JT808Cmd.REQ_8001,
                msgSerialnumber = 0,
                pEncryptFlag = 0,
                pSerialnumber = 1,
                pSubFlag = 0,
                pTotal = 1,
                simNumber = phone.ToBCD(),
            });

            //通过socket发送buffer数据包到终端
        }

        /// <summary>
        /// 解析终端反馈的鉴权应答包
        /// </summary>
        /// <param name="buffer"></param>
        private static void InterfaceParser(byte[] buffer)
        {
            IPacketProvider provider = PacketProvider.CreateProvider();
            //1、泛型解析
            IRspPacketFrom parser = new RspPacketFrom(provider);
            var result = parser.Decode<PB0102>(buffer);
        }

        private static void ManualParser(byte[] buffer)
        {
            IPacketProvider pConvert = PacketProvider.CreateProvider();

            //解析终端发回的数据包,指定方式解析
            //2、指定消息Id解析
            PacketMessage msg = pConvert.Decode(buffer, 0, buffer.Length);
            //解析消息体内容
            if (msg.pmPacketHead.phMessageId == JT808Cmd.RSP_0102)
            {
                PB0102 bodyInfo = new REP_0102().Decode(msg.pmMessageBody);
            }
            else if (msg.pmPacketHead.phMessageId == JT808Cmd.RSP_0100)
            {
                PB0100 bodyinfo = new REP_0100().Decode(msg.pmMessageBody);
            }
        }
    }
}
